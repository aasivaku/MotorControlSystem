<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="customptypemosfet">
<packages>
<package name="TO220-3_ONS">
<pad name="1" x="0" y="0" drill="1.143" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="1.143" diameter="1.651"/>
<pad name="3" x="5.08" y="0" drill="1.143" diameter="1.651"/>
<wire x1="-2.5908" y1="1.27" x2="7.6708" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.7432" y1="-2.54" x2="7.8232" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.8232" y1="-2.54" x2="7.8232" y2="2.54" width="0.1524" layer="21"/>
<wire x1="7.8232" y1="2.54" x2="-2.7432" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-2.7432" y1="2.54" x2="-2.7432" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.3688" y1="0" x2="-4.6228" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-4.6228" y1="0" x2="-4.3688" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.5908" y1="1.143" x2="7.6708" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-2.5908" y1="-2.413" x2="7.6708" y2="-2.413" width="0.1524" layer="51"/>
<wire x1="7.6708" y1="-2.413" x2="7.6708" y2="2.413" width="0.1524" layer="51"/>
<wire x1="7.6708" y1="2.413" x2="-2.5908" y2="2.413" width="0.1524" layer="51"/>
<wire x1="-2.5908" y1="2.413" x2="-2.5908" y2="-2.413" width="0.1524" layer="51"/>
<wire x1="-2.2352" y1="0" x2="-2.4892" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-2.4892" y1="0" x2="-2.2352" y2="0" width="0" layer="51" curve="-180"/>
<text x="1.6764" y="1.4732" size="0.635" layer="51" ratio="4" rot="SR0">TAB</text>
<text x="-0.7366" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="0.8128" y="-0.635" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="FQP9P25">
<pin name="G" x="2.54" y="0" length="middle" direction="pas"/>
<pin name="D" x="38.1" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="S" x="38.1" y="0" length="middle" direction="pas" rot="R180"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="33.02" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="33.02" y1="-7.62" x2="33.02" y2="5.08" width="0.1524" layer="94"/>
<wire x1="33.02" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<text x="15.5956" y="9.1186" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="14.9606" y="6.5786" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="FQP9P25" prefix="U">
<gates>
<gate name="A" symbol="FQP9P25" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO220-3_ONS">
<connects>
<connect gate="A" pin="D" pad="2"/>
<connect gate="A" pin="G" pad="1"/>
<connect gate="A" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="FQP9P25FS-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="FQP9P25" constant="no"/>
<attribute name="MFR_NAME" value="onsemi" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="customntypemosfet">
<packages>
<package name="TO-92-3L_5P33X5P2_ONS">
<pad name="1" x="0" y="0" drill="0.8128" diameter="1.3208" shape="square"/>
<pad name="2" x="2.5908" y="0" drill="0.8128" diameter="1.3208"/>
<pad name="3" x="5.207" y="0" drill="0.8128" diameter="1.3208"/>
<wire x1="0.5334" y1="-1.7272" x2="4.6736" y2="-1.7272" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0" x2="-2.032" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.032" y1="0" x2="-1.778" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="5.1562" y1="-0.9906" x2="4.7752" y2="-1.6764" width="0.1524" layer="21" curve="-16"/>
<wire x1="0.0508" y1="0.9906" x2="5.1562" y2="0.9906" width="0.1524" layer="21" curve="-137"/>
<wire x1="0.4318" y1="-1.6764" x2="0.0508" y2="-0.9906" width="0.1524" layer="21" curve="-16"/>
<wire x1="-0.254" y1="0" x2="0.127" y2="0" width="0.1524" layer="51"/>
<wire x1="0.127" y1="0" x2="0.254" y2="0" width="0.1524" layer="51"/>
<wire x1="0" y1="-0.254" x2="0" y2="0.254" width="0.1524" layer="51"/>
<wire x1="0.5334" y1="-1.5748" x2="4.6736" y2="-1.5748" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.127" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="0.127" y1="0" x2="0.381" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="0.5334" y1="-1.5748" x2="4.6736" y2="-1.5748" width="0.1524" layer="51" curve="-255"/>
<text x="-0.6604" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="0.8636" y="-0.635" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="2N7000BU">
<pin name="SOURCE" x="2.54" y="0" length="middle" direction="pas"/>
<pin name="GATE" x="43.18" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="DRAIN" x="43.18" y="0" length="middle" direction="pas" rot="R180"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="38.1" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="38.1" y1="-7.62" x2="38.1" y2="5.08" width="0.1524" layer="94"/>
<wire x1="38.1" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<text x="18.1356" y="9.1186" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="17.5006" y="6.5786" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="2N7000BU" prefix="U">
<gates>
<gate name="A" symbol="2N7000BU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO-92-3L_5P33X5P2_ONS">
<connects>
<connect gate="A" pin="DRAIN" pad="3"/>
<connect gate="A" pin="GATE" pad="2"/>
<connect gate="A" pin="SOURCE" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="2N7000BU-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="2N7000BU" constant="no"/>
<attribute name="MFR_NAME" value="onsemi" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="customswitch">
<packages>
<package name="TO_1M1QEH_EWI">
<pad name="12" x="0.635" y="-9.398" drill="0.762" diameter="0.762"/>
<pad name="11" x="-0.635" y="-9.398" drill="0.762" diameter="0.762"/>
<smd name="3" x="0" y="-9.4" dx="2.54" dy="1.27" layer="1" roundness="100"/>
<pad name="10" x="0.635" y="-4.699" drill="0.762" diameter="0.762"/>
<pad name="9" x="-0.635" y="-4.699" drill="0.762" diameter="0.762"/>
<smd name="2" x="0" y="-4.7" dx="2.54" dy="1.27" layer="1" roundness="100"/>
<pad name="8" x="0.635" y="0" drill="0.762" diameter="0.762"/>
<pad name="7" x="-0.635" y="0" drill="0.762" diameter="0.762"/>
<smd name="1" x="0" y="0" dx="2.54" dy="1.27" layer="1" roundness="100"/>
<pad name="14" x="5.461" y="0" drill="0.762" diameter="0.762"/>
<pad name="13" x="4.191" y="0" drill="0.762" diameter="0.762"/>
<smd name="4" x="4.83" y="0" dx="2.54" dy="1.27" layer="1" roundness="100"/>
<pad name="16" x="5.461" y="-4.699" drill="0.762" diameter="0.762"/>
<pad name="15" x="4.191" y="-4.699" drill="0.762" diameter="0.762"/>
<smd name="5" x="4.830003125" y="-4.7" dx="2.54" dy="1.27" layer="1" roundness="100"/>
<pad name="18" x="5.461" y="-9.398" drill="0.762" diameter="0.762"/>
<pad name="17" x="4.191" y="-9.398" drill="0.762" diameter="0.762"/>
<smd name="6" x="4.830003125" y="-9.4" dx="2.54" dy="1.27" layer="1" roundness="100"/>
<wire x1="-3.302" y1="-11.049" x2="8.128" y2="-11.049" width="0.1524" layer="51"/>
<wire x1="8.128" y1="-11.049" x2="8.128" y2="1.651" width="0.1524" layer="51"/>
<wire x1="8.128" y1="1.651" x2="-3.302" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-3.302" y1="1.651" x2="-3.302" y2="-11.049" width="0.1524" layer="51"/>
<wire x1="0.9652" y1="-4.699" x2="3.8862" y2="-4.699" width="0" layer="51" curve="-180"/>
<wire x1="3.8862" y1="-4.699" x2="0.9652" y2="-4.699" width="0" layer="51" curve="-180"/>
<wire x1="0.381" y1="1.905" x2="-0.381" y2="1.905" width="0.508" layer="51" curve="-180"/>
<wire x1="-0.381" y1="1.905" x2="0.381" y2="1.905" width="0.508" layer="51" curve="-180"/>
<wire x1="-3.429" y1="-11.176" x2="8.255" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-11.176" x2="8.255" y2="1.778" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.778" x2="-3.429" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0" x2="-4.445" y2="0" width="0.508" layer="21" curve="-180"/>
<wire x1="-4.445" y1="0" x2="-3.683" y2="0" width="0.508" layer="21" curve="-180"/>
<wire x1="-3.683" y1="0" x2="-4.445" y2="0" width="0.508" layer="22" curve="-180"/>
<wire x1="-4.445" y1="0" x2="-3.683" y2="0" width="0.508" layer="22" curve="-180"/>
<wire x1="-0.635" y1="-9.398" x2="0.635" y2="-9.398" width="0.762" layer="49"/>
<wire x1="-0.635" y1="-4.699" x2="0.635" y2="-4.699" width="0.762" layer="49"/>
<wire x1="-0.635" y1="0" x2="0.635" y2="0" width="0.762" layer="49"/>
<wire x1="4.191" y1="0" x2="5.461" y2="0" width="0.762" layer="49"/>
<wire x1="4.191" y1="-4.699" x2="5.461" y2="-4.699" width="0.762" layer="49"/>
<wire x1="4.191" y1="-9.398" x2="5.461" y2="-9.398" width="0.762" layer="49"/>
<text x="-0.8636" y="-5.334" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="0.6858" y="-5.334" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="100DP6T1B1M1QEH">
<pin name="OUT_2" x="2.54" y="0" length="middle" direction="out"/>
<pin name="IN" x="2.54" y="-2.54" length="middle" direction="in"/>
<pin name="3" x="2.54" y="-5.08" length="middle" direction="pas"/>
<pin name="OUT_3" x="38.1" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="5" x="38.1" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="OUT" x="38.1" y="0" length="middle" direction="out" rot="R180"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="33.02" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="33.02" y1="-10.16" x2="33.02" y2="5.08" width="0.1524" layer="94"/>
<wire x1="33.02" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<text x="15.5956" y="9.1186" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="14.9606" y="6.5786" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="100DP6T1B1M1QEH" prefix="U">
<gates>
<gate name="A" symbol="100DP6T1B1M1QEH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO_1M1QEH_EWI">
<connects>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="IN" pad="2"/>
<connect gate="A" pin="OUT" pad="6"/>
<connect gate="A" pin="OUT_2" pad="1"/>
<connect gate="A" pin="OUT_3" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="EG2423-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="100DP6T1B1M1QEH" constant="no"/>
<attribute name="MFR_NAME" value="E-Switch" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="customptypemosfet" deviceset="FQP9P25" device=""/>
<part name="U2" library="customptypemosfet" deviceset="FQP9P25" device=""/>
<part name="U3" library="customntypemosfet" deviceset="2N7000BU" device=""/>
<part name="U4" library="customntypemosfet" deviceset="2N7000BU" device=""/>
<part name="U5" library="customswitch" deviceset="100DP6T1B1M1QEH" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="A" x="17.78" y="63.5" smashed="yes">
<attribute name="NAME" x="33.3756" y="72.6186" size="2.0828" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="32.7406" y="70.0786" size="2.0828" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="U2" gate="A" x="96.52" y="63.5" smashed="yes">
<attribute name="NAME" x="112.1156" y="72.6186" size="2.0828" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="111.4806" y="70.0786" size="2.0828" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="U3" gate="A" x="15.24" y="30.48" smashed="yes">
<attribute name="NAME" x="33.3756" y="39.5986" size="2.0828" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="32.7406" y="37.0586" size="2.0828" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="U4" gate="A" x="96.52" y="30.48" smashed="yes">
<attribute name="NAME" x="114.6556" y="39.5986" size="2.0828" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="114.0206" y="37.0586" size="2.0828" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="U5" gate="A" x="58.42" y="86.36" smashed="yes">
<attribute name="NAME" x="74.0156" y="95.4786" size="2.0828" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="73.3806" y="92.9386" size="2.0828" layer="96" ratio="6" rot="SR0"/>
</instance>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
